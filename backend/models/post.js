const mongoose = require('mongoose');

const postSchema = mongoose.Schema({
    title:{ type: String ,required: true},
    company: { type: String ,required: true},
    email: { type: String ,required: true},
    mblno: { type: String ,required: true},
    gender: String,
    designation:{ type: String ,required: true},
    bday: { type: String ,required: true},
});

module.exports = mongoose.model('Post',postSchema);
