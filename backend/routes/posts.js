const express = require("express");

const Post = require("../models/post");

const router = express.Router();
const checkAuth = require('../middleware/check-auth');

router.post('',checkAuth,(req,res,next) =>{

const post = new Post({
  title: req.body.title,
  company: req.body.company,
  email : req.body.email,
  mblno : req.body.mblno,
  gender : req.body.gender,
  designation : req.body.designation,
  bday : req.body.bday,

});
  post.save().then(createPostId=>{
     res.status(201).json({
    message:"Post Added Successfully.",
    postId: createPostId._id
  });

  });
});



router.get('/:id',(req,res,next) =>{
  Post.findById(req.params.id)
  .then(post =>{
    if(post){
      res.status(200).json(post);
    }else{
      res.status(404).json({message: "Page Not Found"});
    }
  });
});


router.put('/:id',checkAuth,(req,res,next) =>{
  const post = new Post({
      _id: req.body.id,
      title : req.body.title,
      company: req.body.company,
      email: req.body.email,
      mblno : req.body.mblno,
      gender : req.body.gender,
      designation : req.body.designation,
      bday : req.body.bday,

  })
  Post.updateOne({_id: req.params.id},post)
  .then(result=>{
    res.status(200).json({ message: 'Update Successfully... '});

  });
});

router.get('',(req,res,next)=>{
  Post.find().then((documents) =>{

    res.status(200).json({
    message:"Data inserted Successfully1",
    posts:documents
  });

  });

});

router.delete('/:id',checkAuth,  (req,res,next) =>{
 Post.deleteOne({_id: req.params.id}).then((result) =>{
   console.log(result);
     res.status(200).json({message: 'Post Deleted!'});

 });

});


module.exports = router;
