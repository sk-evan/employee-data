import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { NgForm } from '@angular/forms';
import { PostsService } from '../posts.service';
import { Post } from '../post.model';
@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
  private mode = 'create';
  private postId: string;
  public post: Post;



  // validates = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  // validates = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$/;
  // num = /^(\+\d{1,3}[- ]?)?\d{10}$/;
  // num = ^[0][1-9]\d{9}$|^[1-9]\d{9}$

  constructor(
    public postsService: PostsService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');
        this.postsService.getPost(this.postId).subscribe(postData => {
          this.post = {
            id: postData._id,
            title: postData.title,
            company: postData.company,
            email: postData.email,
            mblno: postData.mblno,
            gender: postData.gender,
            designation: postData.designation,
            bday: postData.bday,
          };
        });
      } else {
        this.mode = 'create';
      }
    });
  }

  onSavePost(form: NgForm) {

    // if (form.invalid) {
    //   return;
    // }

    if (this.mode === 'create') {
      this.postsService.addPost(
        form.value.title,
        form.value.company,
        form.value.email,
        form.value.mblno,
        form.value.gender,
        form.value.designation,
        form.value.bday

      );

    }
    else {
      this.postsService.updatePost(
        this.postId,
        form.value.title,
        form.value.company,
        form.value.email,
        form.value.mblno,
        form.value.gender,
        form.value.designation,
        form.value.bday
      );
    }
  }
}
