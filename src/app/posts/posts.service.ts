import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Post } from './post.model';

@Injectable({ providedIn: 'root' })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();

  constructor(private http: HttpClient, private router: Router) { }

  getPosts() {
    this.http.get<{ message: string, posts: any }>('http://localhost:3000/posts')
    .pipe(map((postData) => {
      return postData.posts.map((post) => {
        return {
            title: post.title,
            company: post.company,
            id: post._id,
            email: post.email,
            mblno: post.mblno,
            gender: post.gender,
            designation: post.designation,
            bday: post.bday
        };
      });
    }))
      .subscribe((transformPost) => {
        this.posts = transformPost;
        this.postsUpdated.next([...this.posts]);

      });
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }
  getPost(id: string) {
    return this.http.get<{_id: string, title: string, company: string ,
       email: string , mblno: string , gender: string , designation: string,
       bday: string }>('http://localhost:3000/posts/' + id);
  }

  addPost(title: string, company: string, email: string , mblno: string, gender: string ,
          designation: string, bday: string) {
    const post: Post = { id: null, title, company , email , mblno , gender , designation , bday};

    this.http.post<{ message: string, postId: string }>('http://localhost:3000/posts', post)
    .subscribe((responseData) => {

      const id = responseData.postId;

      post.id = id;
      this.posts.push(post);
      this.postsUpdated.next([...this.posts]);
      this.router.navigate(['/']);

    });


  }

  updatePost(id: string, title: string, company: string, email: string , mblno: string, gender: string,
             designation: string, bday: string ) {
    const post: Post = {id, title, company, email , mblno , gender, designation, bday };
    this.http.put('http://localhost:3000/posts/' + id, post)
    .subscribe(response => {
      const updatedPosts = [...this.posts];
      const oldPostIndex = updatedPosts.findIndex(p => p.id === post.id);

      updatedPosts[oldPostIndex] = post;
      this.posts = updatedPosts;
      this.postsUpdated.next([...this.posts]);
      this.router.navigate(['/']);
    });

  }


  deletePost(postId) {
    this.http.delete('http://localhost:3000/posts/' + postId)
    .subscribe(() => {
      const updatedPosts = this.posts.filter(post => post.id !== postId);
      this.posts = updatedPosts;
      this.postsUpdated.next([...this.posts]);
    });
  }
}
